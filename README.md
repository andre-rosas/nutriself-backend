<h1 align="center" id="top">Backend App <span style="color:#91C787">NutriSelf</span></h1>
<p align="center">App NutriSelf</p>
<br>
<div align="center">
  <img src="https://nutriself-app.vercel.app/static/media/logo-alternativegreen.4b90cbae.png" style="border-radius: 4%" width="400px" alt="Logo nutriself"/>
</div>
<br>
<p align="center">
<h3 align="left">
<span style="color:#91C787"><em>Nutriself</em></span> é uma aplicação de nutrição que tem como objetivo criar uma experiência de usuário personalizada que permite não só um meio para controle nutricional de refeições como a realização de  uma conexão facilitada entre nutricionistas e pacientes.
</h3>

<h3 align="left">Qual a solução que a aplicação traz ?</h3>

<h3 align="left"> 
<span style="color:#91C787"><em>Nutriself</em></span> propõe a criação de uma aplicação em que as pessoas possam consultar valores nutricionais dos alimentos e montar um plano alimentar. Clientes podem se vincular a um nutricionista e estes são capazes de montar planos personalizados e adequados para os clientes que são capazes de fazer seus respectivos acompanhamentos.</h3>
</p>

### <h2>🕹 Features</h2>

- [x] Cadastro de Usuário
- [x] Cadastro de Nutricionista
- [x] Login / Logout
- [x] Criar alimento
- [x] Atualizar Alimento
- [x] Deletar Alimento
- [x] Listar Categorias
- [x] Listar Alimento por Categoria
- [x] Listar Alimento por Nome
- [x] Criar Dieta
- [x] Atualizar Dieta
- [x] Deletar Dieta
- [x] Nutricionista Criar Dieta para o usuário
- [x] Criar Refeição
- [x] Atualizar Refeição
- [x] Deletar Refeição


### <h2>⚠ Pré-requisitos</h2>

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Python](https://www.python.org/), [Postgres](https://www.postgresql.org/)
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/).

### 🎲 Rodando o BackEnd (servidor)

```bash
# Clone este repositório
$ git clone <https://gitlab.com/andre-rosas/nutriself-backend.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd nutriself-backend

# Crie o ambiente virtual
$ python -m venv venv

# Ative o ambiente virtual
$ source venv/bin/activate

# Instale as dependências
$ pip install -r requirements.txt

# Edite o arquivo .env para as suas variáveis de ambiente

# Execute a aplicação
$ flask run

# O servidor inciará na porta:5000 - acesse <http://localhost:5000>
```
### <h2>🍽 FrontEnd </h2>
https://gitlab.com/andre-rosas/nutriself

### <h2>🚦 Endpoints </h2>
https://api-doc-nutriself.vercel.app/


### <h2> 🛠 Tecnologias </h2>
As seguintes ferramentas foram usadas na construção do projeto:

- [DBeaver](https://dbeaver.io)
- [Discord](https://discord.com)
- [Figma](https://www.figma.com)
- [Flask](https://flask.palletsprojects.com/en/2.0.x/)
- [GitHub](https://github.com)
- [Google Docs](https://docs.google.com)
- [Heroku](https://heroku.com)
- [Insomnia](https://insomnia.rest)
- [Postgresql](https://www.postgresql.org/)
- [Python](https://www.python.org/)
- [React](https://pt-br.reactjs.org)
- [Slack](https://slack.com)
- [Trello](https://trello.com)
- [Vercel](https://vercel.com)
- [VSCode](https://code.visualstudio.com)
- [Zoom](https://explore.zoom.us)


### <h2> 📋 Termos de uso </h2>

<p>Este projeto é um projeto <em>opensource</em> com fins educacionais e não possui nenhuma finalidade comercial.</p>

<div align="center">
  <a href="https://choosealicense.com/licenses/mit/" target="_blank"><img src="https://img.shields.io/static/v1?label=License&message=MIT&color=informational"></a>
 </div>

<h2 id="desenvolvedores">🧑‍💻 Equipe de Desenvolvimento</h2>

<div align="center">
<table align="center">
  <tr>
    <td align="center"><a href="https://gitlab.com/osmarn">
      <img src="https://avatars.githubusercontent.com/u/82000434?v=4" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Osmar"/>
      <br />
      <sub><b>Osmar E. Nothaft</b></sub>
      <br />
      <sub><em>Product Owner</em></sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/gutsberserk">
      <img src="https://media-exp1.licdn.com/dms/image/C5603AQHPdNWRu7A6LA/profile-displayphoto-shrink_200_200/0/1637589038106?e=1645056000&v=beta&t=8nV0Ljdnb1Mb5vPvBKDv7TslIh2FBsYYof7odKJdgss" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Lucas Ribeiro"/>
      <br />
      <sub><b>Lucas Ribeiro</b></sub>
      <br />
      <sub><em>Scrum Master</em></sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/andre-rosas">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01SH3TE3UL-63b11c31f2b6-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de André"/>
      <br />
      <sub><b>André Rosas</b></sub>
      <br />
      <sub><em>Tech Leader</em></sub>
      <br />
    </td> 
    <td align="center"><a href="https://gitlab.com/IgorPetersson">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01QNUDCN7M-24007b058eea-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Igor"/>
      <br />
      <sub><b>Igor Cardoso e Santos</b></sub>
      <br />
      <sub><em>Quality Assurance</em></sub>
      <br />
    </td>
    <td align="center"><a href="https://gitlab.com/romulociro">
      <img src="https://ca.slack-edge.com/TQZR39SET-U01K1M2BHS7-e3de19d620af-512" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Rômulo"/>
      <br />
      <sub><b>Rômulo Ciro</b></sub>
      <br />
      <sub><em>Quality Assurance</em></sub>
      <br />
    </td>
</table>
</div>

[Voltar para o topo 🔝](#top)
