"""feat: create tables

Revision ID: 6c1070fba3dc
Revises: 
Create Date: 2021-12-15 12:25:23.331376

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6c1070fba3dc'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('categories',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('category', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('nutritionists',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('full_name', sa.String(length=100), nullable=False),
    sa.Column('email', sa.String(length=80), nullable=False),
    sa.Column('crn', sa.String(), nullable=False),
    sa.Column('spec', sa.String(), nullable=False),
    sa.Column('age', sa.Integer(), nullable=False),
    sa.Column('hour', sa.String(), nullable=False),
    sa.Column('birthday', sa.Date(), nullable=False),
    sa.Column('url_image', sa.String(), nullable=True),
    sa.Column('password_hash', sa.String(length=511), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('crn'),
    sa.UniqueConstraint('email')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(), nullable=True),
    sa.Column('password_hash', sa.String(), nullable=True),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('birthday', sa.DateTime(), nullable=True),
    sa.Column('age', sa.Integer(), nullable=True),
    sa.Column('gender', sa.String(length=50), nullable=True),
    sa.Column('weight', sa.Integer(), nullable=True),
    sa.Column('height', sa.Integer(), nullable=True),
    sa.Column('objective', sa.String(length=50), nullable=True),
    sa.Column('activity', sa.String(length=50), nullable=True),
    sa.Column('basal', sa.Float(), nullable=True),
    sa.Column('url_image', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('diets',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('nutri_id', sa.Integer(), nullable=True),
    sa.Column('description', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['nutri_id'], ['nutritionists.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('foods',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(), nullable=False),
    sa.Column('base_qty', sa.String(), nullable=False),
    sa.Column('base_unit', sa.String(), nullable=False),
    sa.Column('category_id', sa.Integer(), nullable=False),
    sa.Column('protein', sa.Float(), nullable=True),
    sa.Column('lipid', sa.Float(), nullable=True),
    sa.Column('cholesterol', sa.Float(), nullable=True),
    sa.Column('carbohydrate', sa.Float(), nullable=True),
    sa.Column('fiber', sa.Float(), nullable=True),
    sa.Column('calcium', sa.Float(), nullable=True),
    sa.Column('iron', sa.Float(), nullable=True),
    sa.Column('sodium', sa.Float(), nullable=True),
    sa.Column('potassium', sa.Float(), nullable=True),
    sa.Column('energy', sa.Float(), nullable=True),
    sa.ForeignKeyConstraint(['category_id'], ['categories.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('nutri_ratings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('rating', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('nutri_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['nutri_id'], ['nutritionists.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('orders',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('nutri_id', sa.Integer(), nullable=False),
    sa.Column('comments', sa.String(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('status', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['nutri_id'], ['nutritionists.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('meals',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('food_id', sa.Integer(), nullable=True),
    sa.Column('diet_id', sa.Integer(), nullable=True),
    sa.Column('quanty', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['diet_id'], ['diets.id'], ),
    sa.ForeignKeyConstraint(['food_id'], ['foods.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('meals')
    op.drop_table('orders')
    op.drop_table('nutri_ratings')
    op.drop_table('foods')
    op.drop_table('diets')
    op.drop_table('users')
    op.drop_table('nutritionists')
    op.drop_table('categories')
    # ### end Alembic commands ###
