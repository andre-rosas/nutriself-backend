from flask import Flask
from os import getenv
import datetime
from app import routes
from app.configs import database, jwt_auth, migrations, commands, cors


def create_app():
    app = Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = getenv("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["JSON_SORT_KEYS"] = False
    app.config["JWT_SECRET_KEY"] = getenv("JWT_SECRET_KEY")
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=2)

    database.init_app(app)
    migrations.init_app(app)
    commands.init_app(app)
    routes.init_app(app)
    jwt_auth.init_app(app)
    cors.init_app(app)

    return app

#eof