from flask import Blueprint

from app.controllers.order_controller import (
    create_order,
    get_all,
    update_order,
    delete_order
    )

bp_orders = Blueprint("bp_orders", __name__, url_prefix="/orders")

bp_orders.post('')(create_order)
bp_orders.get('')(get_all)
bp_orders.patch('/<int:order_id>')(update_order)
bp_orders.delete('/<int:order_id>')(delete_order)