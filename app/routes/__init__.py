from flask import Flask

def init_app(app: Flask) -> None:
  from app.routes.food_blueprint import bp_foods
  app.register_blueprint(bp_foods)

  from app.routes.user_blueprint import bp_users
  app.register_blueprint(bp_users)

  from app.routes.order_blueprint import bp_orders
  app.register_blueprint(bp_orders)
  
  from app.routes.category_blueprint import bp_categories
  app.register_blueprint(bp_categories)

  from app.routes.diet_blueprint import bp_diet
  app.register_blueprint(bp_diet)
  
  from app.routes.nutritionist_blueprint import bp_nutri
  app.register_blueprint(bp_nutri)
