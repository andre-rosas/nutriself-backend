from flask import Blueprint

from app.controllers.diet_controller import add_food_in_diet, create_diet, delete_diet, get_all_diet_of_user, remove_food_in_diet, update_food_in_meal

bp_diet = Blueprint("bp_diet", __name__)

bp_diet.post("/diet")(create_diet)
bp_diet.get("/diet")(get_all_diet_of_user)
bp_diet.post("/diet/meal/<int:diet_id>")(add_food_in_diet)
bp_diet.delete("/diet/meal/<int:meal_id>")(remove_food_in_diet)
bp_diet.patch("/diet/meal/<int:meal_id>")(update_food_in_meal)
bp_diet.delete("/diet/<int:diet_id>")(delete_diet)