from flask import Blueprint
from app.controllers.nutritionist_controller import (
	create_nutritionist,
	login_nutritionist,
	get_nutritionist,
	update_nutritionist,
	delete_nutritionist,
	get_nutrionist_by_name,
	get_all_nutrionists,
	update_image,
	get_nutri_rating
)


from flask_httpauth import HTTPTokenAuth

auth = HTTPTokenAuth(scheme='Bearer')

bp_nutri = Blueprint('bp_nutri', __name__, url_prefix='/nutri')

bp_nutri.post('/register')(create_nutritionist)
bp_nutri.post('/login')(login_nutritionist)
bp_nutri.get('')(get_nutritionist)
bp_nutri.patch('')(update_nutritionist)
bp_nutri.delete('')(delete_nutritionist)

bp_nutri.get('/all_nutri')(get_all_nutrionists)
bp_nutri.get('/<name>')(get_nutrionist_by_name)

bp_nutri.patch('/image')(update_image)
bp_nutri.get('/rating/<nutri_full_name>')(get_nutri_rating)

#eof