from flask import Blueprint

from app.controllers.user_controller import (
	create,
	get_user,
	update,
	delete,
	login,
	update_password,
	get_users_by_nutri,
	rating,
	update_user_image,
	get_best_ranked_nutri
)

from flask_httpauth import HTTPTokenAuth
auth = HTTPTokenAuth(scheme='Bearer')

bp_users = Blueprint("bp_users",__name__, url_prefix="/users")

bp_users.get("")(get_user)
bp_users.post("")(create)
bp_users.patch("")(update)
bp_users.delete("")(delete)
bp_users.post("/login")(login)
bp_users.post("/password")(update_password)
bp_users.get("/pacientes")(get_users_by_nutri)

bp_users.post('/rating/<int:nutri_id>')(rating)
bp_users.get('/rating')(get_best_ranked_nutri)
bp_users.patch('/image')(update_user_image)

#eof