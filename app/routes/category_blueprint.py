from flask import Blueprint

from app.controllers.category_controller import get_all

bp_categories = Blueprint("bp_categories", __name__)

bp_categories.get("/categories")(get_all)