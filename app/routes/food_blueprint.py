from flask import Blueprint

from app.controllers.food_controller import create_food, delete_food, get_all, get_food_by_category, get_food_by_fat,get_food_by_name, update_food

bp_foods = Blueprint("bp_foods", __name__)

bp_foods.get("/foods")(get_all)
bp_foods.get("/foods/by_name/<name>")(get_food_by_name)
bp_foods.get("/foods/by_category/<category>")(get_food_by_category)
bp_foods.get("/foods/by_fat/<fat>")(get_food_by_fat)
bp_foods.post("/foods")(create_food)
bp_foods.patch("/foods/<id>")(update_food)
bp_foods.delete("/foods/<id>")(delete_food)