from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def init_app(app: Flask):
    db.init_app(app)
    app.db = db

    from app.models.food_model import FoodModel
    from app.models.category_model import CategoryModel
    from app.models.user_model import UserModel
    from app.models.nutritionist_model import NutritionistModel
    from app.models.diet_model import DietModel
    from app.models.meal_model import MealModel
    from app.models.order_model import OrderModel

#eof