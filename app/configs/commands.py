from flask import Flask, current_app
from flask.cli import AppGroup
from json import load
from app.models.food_model import FoodModel
from app.models.category_model import CategoryModel


def read_json(filename: str):
    with open(filename) as j_file:
        return load(j_file)


def cli_foods(app: Flask):
    cli = AppGroup('cli_foods')

    @cli.command("create")
    def cli_foods_create():
        session = current_app.db.session

        data_food = read_json("foodList.json")
        for alimento in data_food:
            ex = {}
            ex['description'] = alimento['description']
            ex['base_qty'] = alimento['base_qty']
            ex['base_unit'] = alimento['base_unit']
            ex['category_id'] = alimento['category_id']

            atributos = ['protein','lipid','cholesterol','carbohydrate','fiber','calcium','iron','sodium','potassium','energy']

            for atributo in alimento['attributes']:
                if atributo in atributos:
                    if atributo == 'energy':
                        if type(alimento['attributes']['energy']['kcal']) != float and type(alimento['attributes']['energy']['kcal']) != int:
                              p1 = FoodModel(description=ex['description'], base_qty=ex['base_qty'], base_unit=ex['base_unit'],
                                category_id=ex['category_id'])
                        else:
                            ex[atributo] = alimento["attributes"][atributo]['kcal']

                    elif type(alimento["attributes"][atributo]['qty']) != float and type(alimento["attributes"][atributo]['qty']) != int:
                        p1 = FoodModel(description=ex['description'], base_qty=ex['base_qty'], base_unit=ex['base_unit'],
                                category_id=ex['category_id'])
                    else:
                        ex[atributo] = alimento["attributes"][atributo]['qty']
                
            p1 = FoodModel(**ex)
            
            session.add(p1)
            
        session.commit()

    app.cli.add_command(cli)

def cli_categories(app: Flask):
    cli = AppGroup('cli_categories')

    @cli.command("create")
    def cli_categories_create():
        session = current_app.db.session

        data = read_json("category.json")

        for item in data:
            p1 = CategoryModel(id=item['id'],category=item['category'])
            session.add(p1)

        session.commit()

    app.cli.add_command(cli)


def init_app(app: Flask):
    cli_foods(app)
    cli_categories(app)
