from app.configs.database import db
from sqlalchemy import Column, Integer, ForeignKey
from dataclasses import dataclass

@dataclass
class NutriRatingModel(db.Model):

	'''A model to rate Nutritionist linking users and nutritionists'''

	rating: int

	__tablename__ = 'nutri_ratings'

	id = Column(Integer, primary_key=True)
	rating = Column(Integer, nullable=False)

	user_id = Column(Integer, ForeignKey('users.id'))
	nutri_id = Column(Integer, ForeignKey('nutritionists.id'))

#eof