from app.configs.database import db
class DietModel(db.Model):
    __tablename__ = 'diets'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(
      db.Integer,
      db.ForeignKey("users.id"),
      nullable=False
      )

    nutri_id = db.Column(
      db.Integer,
      db.ForeignKey("nutritionists.id")
      )

    description = db.Column(db.String, nullable=False)

#eof