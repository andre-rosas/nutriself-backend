from app.configs.database import db


class FoodModel(db.Model):
    __tablename__ = 'foods'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String, nullable=False)
    base_qty = db.Column(db.String, nullable=False)
    base_unit = db.Column(db.String, nullable=False)

    category_id = db.Column(
      db.Integer,
      db.ForeignKey("categories.id"),
      nullable=False
      )

    categoria = db.relationship(
      "CategoryModel",
      backref=db.backref("food", uselist=False)
    )

    protein = db.Column(db.Float)
    lipid = db.Column(db.Float)
    cholesterol = db.Column(db.Float)
    carbohydrate = db.Column(db.Float)
    fiber = db.Column(db.Float)
    calcium = db.Column(db.Float)
    iron = db.Column(db.Float)
    sodium = db.Column(db.Float)
    potassium = db.Column(db.Float)
    energy = db.Column(db.Float)

#eof