from app.configs.database import db


class MealModel(db.Model):
    __tablename__ = 'meals'
    id = db.Column(db.Integer, primary_key=True)
    food_id = db.Column(db.Integer, db.ForeignKey('foods.id'))
    diet_id = db.Column(db.Integer, db.ForeignKey('diets.id'))
    quanty = db.Column(db.Integer)

#eof