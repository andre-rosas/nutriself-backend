from app.configs.database import db
from sqlalchemy import Column, Integer, String, Float, DateTime
from werkzeug.security import generate_password_hash, check_password_hash
from dataclasses import dataclass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib, ssl
from os import environ
from dotenv import load_dotenv

@dataclass
class UserModel(db.Model):

    '''A model for user'''

    id: int
    email: str
    name: str
    birthday: str
    age: int
    gender: str
    weight: int
    height: int
    objective: str
    activity: str
    basal: float
    url_image: str

    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=True, unique=True)
    password_hash = Column(String, nullable=True)
    name = Column(String(100), nullable=True)
    birthday = Column(DateTime, nullable=True)
    age = Column(Integer, nullable=True)
    gender = Column(String(50), nullable=True)
    weight = Column(Integer, nullable=True)
    height = Column(Integer, nullable=True)
    objective = Column(String(50), nullable=True)
    activity = Column(String(50), nullable=True)
    basal = Column(Float, nullable=True)
    url_image = Column(String, nullable=True)

    refeicoes = db.relationship("DietModel", backref="user", uselist=True)
    
    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
    
    @staticmethod
    def send_email(email_to, title, user, comment):
        load_dotenv()
        
        email = MIMEMultipart('alternative')
    
        password = environ.get('PASSWORD')
        email["From"] = environ.get("EMAIL_FROM")
        email["To"] = email_to 
        email["Subject"] = title 
        style = "{background-color: #4CAF50;border: none;color: white;padding: 10px 26px;text-align: center;text-decoration: none;display: inline-block;font-size: 12px;margin: 4px 2px;cursor: pointer;border-radius:10px;}"

        html =f"""\
            <html>
            <head> <style>
                .button {style}
            </style>
            <h4>Novo pedido de dieta de {user}.</h4></head>
            <body>
                <h4>Olá!<br>
                Como você está?
                </h4>
                <h4>Sobre a dieta : {comment}.</h4>
                <br>
                <h3 style="color:green;">Click <a href="nutriself.vercel.app"><button class="button" type="submit" style="color:blue;">AQUI</button></a> para vê-la em nossa plataforma :D.</h3>
                <br>
                <h4>Agradecemos pela preferência,<br> Equipe NutriSelf &#127823; !</h4>
            </body>
            </html>
            """
        
        email.attach(MIMEText(html, 'html'))
        
        context = ssl.create_default_context()
        
        with smtplib.SMTP_SSL("smtp.gmail.com", port=465, context=context) as server:
            server.login(email["From"], password)
            server.sendmail(email["From"], email["To"], email.as_string().encode('utf-8'))
            server.quit()

#eof