from sqlalchemy.sql.sqltypes import Date
from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String
from werkzeug.security import generate_password_hash, check_password_hash
import re

@dataclass
class NutritionistModel(db.Model):

	''' A model for Nutritionist'''
	
	id: int
	full_name: str
	email: str
	crn: str
	spec: str
	age: int
	hour: str
	birthday: str
	url_image: str
	# password_hash: str


	__tablename__ = "nutritionists"

	id = Column(Integer, primary_key=True)
	full_name = Column(String(100), nullable=False)
	email = Column(String(80), nullable=False, unique=True)
	crn = Column(String, nullable=False, unique=True)
	spec = Column(String, nullable=False)
	age = Column(Integer, nullable=False)
	hour = Column(String, nullable=False)
	birthday = Column(Date(), nullable=False)
	url_image = Column(String, nullable=True)
	password_hash = Column(String(511), nullable=False)

	@staticmethod
	def CRNPatternizer(crn):

		crn_pattern = r"^[0-9]{4}(-[P|S]){0,1}$"

		if re.fullmatch(crn_pattern, crn, flags=0):
			return True
	
	@staticmethod
	def BirthDayPatternizer(birthday):

		birthday_pattern = r"^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$"

		if re.fullmatch(birthday_pattern, birthday, flags=0):
			return True

	@property
	def password(self):
		raise AttributeError('Password cannot be accessed!')

	@password.setter
	def password(self, password_to_hash):
		self.password_hash = generate_password_hash(password_to_hash)

	def verify_password(self, password_to_compare):
		return check_password_hash(self.password_hash, password_to_compare)

#eof