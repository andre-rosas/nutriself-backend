from app.configs.database import db
from dataclasses import dataclass
from app.exceptions.order_exceptions import WrongKeysSended, ConflitError
from app.models.nutritionist_model import NutritionistModel
from app.models.user_model import UserModel


@dataclass
class OrderModel(db.Model):
    __tablename__ = 'orders'

    id:int
    user_id:int
    nutri_id:int
    comments:str
    created_at:str
    status:str

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
      db.Integer,
      db.ForeignKey("users.id"),
      nullable=False
      )
    nutri_id = db.Column(
      db.Integer,
      db.ForeignKey("nutritionists.id"),
      nullable=False
      )
    comments = db.Column(db.String, nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False)
    status = db.Column(db.String, nullable=False)


    user = db.relationship(
      "UserModel",
      backref=db.backref("User", uselist=False)
    )

    nutri = db.relationship(
      "NutritionistModel",
      backref=db.backref("Nutritionist", uselist=False)
    )

    @staticmethod
    def validate(**kwargs):
      orders_keys = ['user_id','nutri_id', 'comments', 'status']
      orders_keys_int = ['user_id','nutri_id']
      orders_keys_str = ['comments', 'status']
      status = ['waiting', 'accepted', 'rejected']
                
      for key in orders_keys:
        if key not in kwargs:
          raise WrongKeysSended(f"The keys needed are: {', '.join(orders_keys)}!")
        for key in orders_keys_int:
          if type(kwargs[key]) != int:
            raise WrongKeysSended(f"For {orders_keys_int} keys data type must be Integer!")
        for key in orders_keys_str:
          if type(kwargs[key]) != str:
            raise WrongKeysSended(f"For {', '.join(orders_keys_str)} keys data type must be String!")
        if kwargs['status'] not in status:
          raise WrongKeysSended(f"status key can be: {', '.join(status)}!")
        if not NutritionistModel.query.get(kwargs['nutri_id']):
          raise WrongKeysSended(f"nutri_id not found!")
        if not UserModel.query.get(kwargs['user_id']):
          raise WrongKeysSended(f"user_id not found!")
        
      valid_entry = {key:kwargs[key] for key in orders_keys if key in orders_keys}

      return valid_entry


    @staticmethod
    def validate_update(**kwargs):
      update_keys = ['comments','status']      
      status = ['waiting', 'accepted', 'rejected']

      for key in update_keys:
        if key not in kwargs:
          raise WrongKeysSended(f"The key needed is: {', '.join(update_keys)}!")
        if type(kwargs[key]) != str:
          raise WrongKeysSended(f"Data type must be String!")
      if not kwargs['status']:
          raise WrongKeysSended(f"status key is missing!")
      if not kwargs['comments']:
          raise WrongKeysSended(f"comments key is missing!")
    
