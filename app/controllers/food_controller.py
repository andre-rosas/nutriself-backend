from flask import request, current_app, jsonify
from app.models.category_model import CategoryModel
from app.models.food_model import FoodModel
from sqlalchemy import exc

def get_all():
    foods = FoodModel.query.all()

    return jsonify([{
        "id": item.id,
        "description": item.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == item.category_id).all()[0].category,
        "base_qty":item.base_qty,
        'base_unit':item.base_unit,
        'protein':item.protein,
        'lipid':item.lipid,
        'cholesterol':item.cholesterol,
        'carbohydrate':item.carbohydrate,
        'fiber':item.fiber,
        'calcium':item.calcium,
        'iron':item.iron,
        'sodium':item.sodium,
        'potassium':item.potassium,
        'energy':item.energy,
      }for item in foods]), 200

def get_food_by_name(name):
    foods = FoodModel.query.filter(FoodModel.description.ilike(f'%{name}%'))

    return jsonify([{
        "id": item.id,
        "description": item.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == item.category_id).all()[0].category,
        "base_qty":item.base_qty,
        'base_unit':item.base_unit,
        'protein':item.protein,
        'lipid':item.lipid,
        'cholesterol':item.cholesterol,
        'carbohydrate':item.carbohydrate,
        'fiber':item.fiber,
        'calcium':item.calcium,
        'iron':item.iron,
        'sodium':item.sodium,
        'potassium':item.potassium,
        'energy':item.energy,
      }for item in foods]), 200

def get_food_by_category(category):

    foods = FoodModel.query.select_from(FoodModel).join(CategoryModel).filter(CategoryModel.category.ilike(category)).all()

    if foods == []:
        return {"msg": "Category not found."},400

    return jsonify([{
        "id": item.id,
        "description": item.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == item.category_id).all()[0].category,
        "base_qty":item.base_qty,
        'base_unit':item.base_unit,
        'protein':item.protein,
        'lipid':item.lipid,
        'cholesterol':item.cholesterol,
        'carbohydrate':item.carbohydrate,
        'fiber':item.fiber,
        'calcium':item.calcium,
        'iron':item.iron,
        'sodium':item.sodium,
        'potassium':item.potassium,
        'energy':item.energy,
      }for item in foods]), 200

def get_food_by_fat(fat):
    try:
        foods = FoodModel.query.filter(FoodModel.lipid == fat).all()
    except exc.DataError:
        return {"err":"Only numbers is accepted."},400

    if foods == []:
        return {"msg": "Fat not found."},400

    return jsonify([{
        "id": item.id,
        "description": item.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == item.category_id).all()[0].category,
        "base_qty":item.base_qty,
        'base_unit':item.base_unit,
        'protein':item.protein,
        'lipid':item.lipid,
        'cholesterol':item.cholesterol,
        'carbohydrate':item.carbohydrate,
        'fiber':item.fiber,
        'calcium':item.calcium,
        'iron':item.iron,
        'sodium':item.sodium,
        'potassium':item.potassium,
        'energy':item.energy,
      }for item in foods]), 200

def create_food():
    try:
        data = request.get_json()
        newfood = FoodModel(**data)
        
        current_app.db.session.add(newfood)
        current_app.db.session.commit()
        
    except TypeError:
        return {"err": "Only the following keys are accepted. = id,description,category,base_qty,base_unit,protein,lipid,cholesterol,carbohydrate,fiber,calcium,iron,sodium,potassium,energy"},404
    
    except exc.IntegrityError:
        return {"err": "Value not allowed."},404

    return jsonify({
        "id": newfood.id,
        "description": newfood.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == newfood.category_id).all()[0].category,
        "base_qty":newfood.base_qty,
        'base_unit':newfood.base_unit,
        'protein':newfood.protein,
        'lipid':newfood.lipid,
        'cholesterol':newfood.cholesterol,
        'carbohydrate':newfood.carbohydrate,
        'fiber':newfood.fiber,
        'calcium':newfood.calcium,
        'iron':newfood.iron,
        'sodium':newfood.sodium,
        'potassium':newfood.potassium,
        'energy':newfood.energy,
    }), 201

def update_food(id):
    data = request.get_json()
    keys = ['description','category_id','base_qty','base_unit','protein','lipid','cholesterol','carbohydrate','fiber','calcium','iron','sodium','potassium','energy']
    try:
        newfood = (
        FoodModel
        .query
        .filter(FoodModel.id == id)
        .first()
        )

        for key,value in data.items():
            if key not in keys:
                return {"err": "Only the following keys are accepted. = id,description,category,base_qty,base_unit,protein,lipid,cholesterol,carbohydrate,fiber,calcium,iron,sodium,potassium,energy"},404
            if key == 'category_id' and value > 15:
                return {"err": "Non-existent category."},404
            setattr(newfood, key, value)

        return jsonify({
            "id": newfood.id,
            "description": newfood.description,
            'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == newfood.category_id).all()[0].category,
            "base_qty":newfood.base_qty,
            'base_unit':newfood.base_unit,
            'protein':newfood.protein,
            'lipid':newfood.lipid,
            'cholesterol':newfood.cholesterol,
            'carbohydrate':newfood.carbohydrate,
            'fiber':newfood.fiber,
            'calcium':newfood.calcium,
            'iron':newfood.iron,
            'sodium':newfood.sodium,
            'potassium':newfood.potassium,
            'energy':newfood.energy,
        }), 201

    except exc.DataError:
        return {"err": "Value not allowed."},404

    except AttributeError:
        return {"err": "ID food not found."},404


def delete_food(id):
    query = FoodModel.query.get(id)

    if query == None:
      return {"err": "Food not found!"},404

    current_app.db.session.delete(query)
    current_app.db.session.commit()

    return "", 204
