from re import fullmatch
from flask import jsonify, request, current_app
from sqlalchemy import desc, func
from app.exceptions.user_exceptions import KeyNotAuthorized, DateFormatError, EmailAleadyExists, GenderError, KeysMissing, KeysTypeErrors, UserNotAuthorized
from app.models.nutri_rating_model import NutriRatingModel
from app.models.nutritionist_model import NutritionistModel
from app.models.user_model import UserModel
from app.models.diet_model import DietModel
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from datetime import date
from http import HTTPStatus
import sqlalchemy

def validate_user(user):

    important_keys = ["email", "name", "birthday", "gender", "weight", "height", "objective", "activity", "age", "id", "basal"]
    for key in important_keys:
        if key not in user.keys():
            print(key)
            raise UserNotAuthorized("Not Authorized")

def validate_nutri(nutri):

    important_keys = ["id", "full_name", "email", "crn", "spec", "age", "hour", "birthday"]
    for key in important_keys:
        if key not in nutri.keys():
            raise UserNotAuthorized("Not Authorized")

def validate_create(data):
    
    important_keys = ["email", "password", "name", "birthday", "gender", "weight", "height", "objective", "activity"]
    keys_missing = []
    for key in important_keys:
        if key not in data.keys():
            keys_missing.append(key)
    
    if len(keys_missing)>0:
        message = "Keys missing: " + ', '.join(keys_missing) +"."
        raise KeysMissing(message)

    type_str = ["email", "password", "name", "birthday", "gender", "objective", "activity"]
    type_int = ["weight","height"]

    message = ["email: str", "password: str", "name: str", "birthday: str", "gender: str", "weight: int", "height: int", "objective: str", "activity: str"]

    for key in type_str:
        if type(data[key]) != str:
            raise KeysTypeErrors("The type of keys must be:" + ', '.join(message))
    
    for key in type_int:
        if type(data[key]) != int:
            raise KeysTypeErrors("The keys must be:" + ', '.join(message))


    if data["gender"] != "female" and data["gender"] != "male" and data["gender"] != "other":
        raise GenderError("Gender can only be male, female or other")
    
    if bool(fullmatch("[0-9]{4}[-][0-9]{2}[-][0-9]{2}", data["birthday"])) == False:
        raise DateFormatError("The date of birth must be exactly in this format: YYYY-MM-DD")

    users = UserModel.query.all()
    users_email = [user.email for user in users]

    if data["email"] in users_email:
        raise EmailAleadyExists("This email is already registered in the system")

def validate_update(data):

    important_keys = ["password", "weight", "height", "objective", "activity"]
    for key in data.keys():
        if key not in important_keys:
            raise KeyNotAuthorized("Only keys can change: " + ', '.join(important_keys))

    type_str = ["objective","activity"]
    type_int = ["weight","height"]

    message = ["weight: int","height: int","objective: str","activity: str"]

    for key in type_str:
        if key in data.keys():
            if type(data[key]) != str:
                raise KeysTypeErrors("The keys must be:" + ', '.join(message))
    
    for key in type_int:
        if key in data.keys():
            if type(data[key]) != int:
                raise KeysTypeErrors("The keys must be:" + ', '.join(message))

def calculate_age(birthday):

    today = date.today()
    
    year = int(birthday[0:4])
    month = int(birthday[5:7])
    day = int(birthday[-2:])

    if month < today.month:
        age = today.year - year
    elif month > today.month:
        age = today.year - year -1
    elif month == today.month:
        if day <= today.day:
            age = today.year - year
        else:
            age = today.year - year -1

    return age

def calculate_basal(age, weight, height, gender):

    if gender == "male":
        return round(66.4730+(weight*13.752)+(5.003*height)-(6.755*age), 2)
    if gender == "female":
        return round(665.095+(weight*9.5630)+(1.850*height)-(4.678*age), 2)
    else:
        return round(365.784+(weight*11.6575)+(3.4265*height)-(5.7165*age), 2)

#-------------------------------------------------

def create():

    try:
        data = request.get_json()
        validate_create(data)

        data['age'] = calculate_age(data["birthday"])

        data['basal'] = calculate_basal(data["age"],data["weight"],data["height"], data["gender"])
        password_to_hash = data.pop("password")

        new_user = UserModel(**data)
        new_user.password = password_to_hash
        current_app.db.session.add(new_user)
        current_app.db.session.commit()

        return jsonify(new_user),HTTPStatus.CREATED # <-- 201

    except KeysMissing as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400
    except KeysTypeErrors as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400
    except DateFormatError as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400
    except EmailAleadyExists as e:
        return {"message": str(e)}, HTTPStatus.CONFLICT # <-- 409
    except GenderError as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400

@jwt_required()
def get_user():

    try:
        user = get_jwt_identity()
        validate_user(user)
        return jsonify(user), HTTPStatus.OK # <-- 200

    except UserNotAuthorized as e:
        return {"message": str(e)}, HTTPStatus.UNAUTHORIZED # <-- 401

@jwt_required()
def delete():

    try:
        user_delete = get_jwt_identity()
        validate_user(user_delete)
        user = UserModel.query.get(user_delete["id"])
        current_app.db.session.delete(user)
        current_app.db.session.commit()
        return "", HTTPStatus.NO_CONTENT # <-- 204

    except UserNotAuthorized as e:
        return {"message": str(e)}, HTTPStatus.UNAUTHORIZED # <-- 401

@jwt_required()  
def update():

    try:
        data = request.get_json()
        validate_update(data)
        user_update = get_jwt_identity()
        validate_user(user_update)
        user = UserModel.query.get(user_update['id'])
        UserModel.query.filter_by(id=user.id).update(data)
        current_app.db.session.commit()
        return "", HTTPStatus.NO_CONTENT # <-- 204

    except KeyNotAuthorized as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400
    except KeysTypeErrors as e:
        return {"message": str(e)}, HTTPStatus.BAD_REQUEST # <-- 400
    except UserNotAuthorized as e:
        return {"message": str(e)}, HTTPStatus.UNAUTHORIZED # <-- 401
   
def login():

    try:
        data = request.get_json()
        user = UserModel.query.filter_by(email=data['email']).first()

        if not user.verify_password(data["password"]):
            return {"message": "Email and password combination does not match"}, HTTPStatus.UNAUTHORIZED # <-- 401

        if user.verify_password(data['password']):
            access_token = create_access_token(identity=user)
            return {'accessToken': access_token}, HTTPStatus.OK # <-- 200

    except:
        return {"message": "The email and password fields are mandatory and only they must be informed"}, HTTPStatus.NOT_FOUND # <-- 401

@jwt_required()
def update_password():

    try:
        new_password = request.get_json()
        user_update = get_jwt_identity()
        validate_user(user_update)
        user = UserModel.query.get(user_update["id"])
        user.password = str(new_password["password"])
        current_app.db.session.commit()
        return "", HTTPStatus.NO_CONTENT # <-- 204

    except UserNotAuthorized as e:
        return {"message": str(e)}, HTTPStatus.UNAUTHORIZED # <-- 401
    except:
        return {"message": "The key 'password' is mandatory"}, HTTPStatus.BAD_REQUEST # <-- 400
    
@jwt_required()
def get_users_by_nutri():

    try:
        nutri = get_jwt_identity()
        validate_nutri(nutri)
        users_patients = current_app.db.session.query(UserModel).select_from(UserModel).join(DietModel).filter(DietModel.nutri_id == nutri['id']).all()
        return jsonify({
            "patients":[patient for patient in users_patients]
            }), HTTPStatus.OK # <-- 200
    except UserNotAuthorized as e:

        return {"message": str(e)}, HTTPStatus.UNAUTHORIZED # <-- 401


@jwt_required()
def rating(nutri_id: int):

    data = request.get_json()

    if len(data) > 1:
        return {'message': 'Only rating field is allowed'}, HTTPStatus.BAD_REQUEST # <-- 400
    
    if not data['rating'] in [1, 2, 3, 4, 5]:
        return {'error': 'Ratings must be between 1 and 5'}, HTTPStatus.BAD_REQUEST # <-- 400

    user_found = get_jwt_identity()

    user_rating = NutriRatingModel.query.all()
    print(user_rating)

    users_rating_list = [item.rating for item in user_rating]

    if data["rating"] in users_rating_list:
        return {'message': 'You already rate this nutritionist.'}, HTTPStatus.BAD_REQUEST # <-- 400

    rating = NutriRatingModel(rating=data['rating'], user_id = user_found['id'], nutri_id = nutri_id)
    
    current_app.db.session.add(rating)
    current_app.db.session.commit()

    return jsonify(rating), HTTPStatus.CREATED # <-- 201

@jwt_required()
def update_user_image():

	''' Update image of avatar '''

	found_user = get_jwt_identity()

	user_data = request.get_json()

	try:
		user_update = (
			UserModel
			.query
			.filter(UserModel.email == found_user['email'])
			.first()
		)

		url_image = found_user.pop('url_image')
		user_update.url_image = url_image

		UserModel.query.filter(UserModel.email == found_user['email']).update(user_data)
		current_app.db.session.commit()

		return jsonify(user_update), HTTPStatus.OK # <-- 200

	except sqlalchemy.exc.InvalidRequestError:
		return {'error': 'Invalid fields.'}, HTTPStatus.BAD_REQUEST # <-- 400
	except KeyError as error:
		return {'error': f'You are missing this argument: {error.args[0]}.'}, HTTPStatus.BAD_REQUEST # <-- 400


def get_best_ranked_nutri():
	
	session = current_app.db.session

	ranked_rows = session.query(NutritionistModel.full_name, NutritionistModel.url_image, NutritionistModel.spec, func.avg(NutriRatingModel.rating))\
		.join(NutriRatingModel, NutriRatingModel.nutri_id == NutritionistModel.id)\
		.group_by(NutritionistModel.full_name, NutritionistModel.url_image, NutritionistModel.spec)\
		.order_by(desc(func.avg(NutriRatingModel.rating)))\
		.limit(10)\
		.all()
    

	data = [{'full_name': row[0], 'url_image': row[1], 'spec': row[2], 'rating': round(float(row[3]),2)} for row in ranked_rows]

	return jsonify(data), HTTPStatus.OK # <-- 200

#eof
