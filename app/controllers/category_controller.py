from flask import jsonify
from app.models.category_model import CategoryModel

def get_all():
    categ = CategoryModel.query.all()

    return jsonify([{
        "id": item.id,
        "category":item.category
      }for item in categ]), 200
