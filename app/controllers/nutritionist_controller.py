from flask import request, current_app, jsonify
from app.controllers.user_controller import rating
from app.exceptions.nutritionist_exceptions import EmptyListError, MissingCharactersError, MoreCharactersthanNecessary, NutritionistNotFoundError, WrongTypeError
from app.models.nutri_rating_model import NutriRatingModel
from app.models.nutritionist_model import NutritionistModel
from http import HTTPStatus
import sqlalchemy
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
from functools import reduce
from sqlalchemy import desc, func
from datetime import date



def calculate_age(birthday):

	'''Calculate age of a nutritionist'''

	today = date.today()

	year = int(birthday[0:4])
	month = int(birthday[5:7])
	day = int(birthday[-2:])

	if month < today.month:
		age = today.year - year
	elif month > today.month:
		age = today.year - year -1
	elif month == today.month:
		if day <= today.day:
			age = today.year - year
		else:
			age = today.year - year -1

	return age

def create_nutritionist():

	''' Create a Nutritionist for register'''

	data = request.get_json()

	important_keys = ["full_name", "email","crn", "spec", "hour","birthday", "password"]
	missing_keys = []
	for key in important_keys:
		if key not in data.keys():
			missing_keys.append(key)
	
	if len(missing_keys) > 0:
		message = "Missing Keys: " + ', '.join(missing_keys) + "."
		return {'error': message}, HTTPStatus.BAD_REQUEST # <-- 400

	try:
		nutri_data = {
			"full_name": data['full_name'],
			"email": data['email'],
			"crn": data['crn'],
			"spec": data['spec'],
			"age": calculate_age(data["birthday"]),
			"hour": data['hour'],
			"birthday": str(data['birthday']),
			"password": data['password']
		}

		if len(data['crn']) < 4:
			raise MissingCharactersError
		
		if len(data['crn']) > 6:
			raise MoreCharactersthanNecessary

		if type(nutri_data['age']) != int:
			raise WrongTypeError

		if not NutritionistModel.CRNPatternizer(data['crn']):
			return {'error': 'Insert crn in this format: xxxx or xxxx-P or xxxx-S'}, HTTPStatus.BAD_REQUEST # <-- 400

		if not NutritionistModel.BirthDayPatternizer(data['birthday']):
			return {'error': 'Insert birthday in this format: YYYY-MM-DD'}, HTTPStatus.BAD_REQUEST # <-- 400

		password_to_hash = nutri_data.pop("password")

		new_nutri = NutritionistModel(**nutri_data)
		new_nutri.password = password_to_hash

		current_app.db.session.add(new_nutri)
		current_app.db.session.commit()

		serializer = {
			"id": new_nutri.id,
			"full_name": new_nutri.full_name,
			"email": new_nutri.email,
			"crn": new_nutri.crn,
			"spec": new_nutri.spec,
			"age": new_nutri.age,
			"hour": new_nutri.hour,
			"birthday": str(new_nutri.birthday)
		}

		return jsonify(serializer), HTTPStatus.CREATED # <-- 201

	except TypeError as error:
		return {'error': {error.args[0]}}, HTTPStatus.BAD_REQUEST # <-- 400
	except WrongTypeError as error:
		return {'error': 'age should be an Integer.'}
	except MissingCharactersError as error:
		return {'message': 'You are missing numbers at CRN!'}, HTTPStatus.BAD_REQUEST # <-- 400
	except MoreCharactersthanNecessary as error:
		return {'message': 'You put more characters than necessary at CRN!'}, HTTPStatus.BAD_REQUEST # <-- 400
	except sqlalchemy.exc.IntegrityError as error:
		return {'error': f'Key email {new_nutri.email} OR crn {new_nutri.crn} already exists in database.'}, HTTPStatus.CONFLICT # <-- 409
	except sqlalchemy.exc.ProgrammingError as error:
		return {'error': error.args[0]}, HTTPStatus.BAD_REQUEST # <-- 400

def login_nutritionist():

	'''Login of Nutritionist. The answer should be an acess_token.'''

	nutri_data = request.get_json()

	try:
		found_nutri = (
			NutritionistModel
			.query
			.filter(NutritionistModel.email == nutri_data['email'])
			.first()
		)

		if not found_nutri:
			return {'message': 'User not found'}, HTTPStatus.NOT_FOUND # <-- 404
		
		if not found_nutri.verify_password(nutri_data["password"]):
			return jsonify({"message": "Unauthorized. Incorrect password."}), HTTPStatus.UNAUTHORIZED # <-- 401
	
		access_token = create_access_token(identity=found_nutri)

		return jsonify({'accessToken': access_token}), HTTPStatus.OK # <--200

	except KeyError as error:
		return {'error': f'You are missing this argument: {error.args[0]}.'}, HTTPStatus.BAD_REQUEST # <-- 400

@jwt_required()
def get_nutritionist():

	''' Get a logged Nutritionist with token'''

	current_nutri = get_jwt_identity()

	serializer = {
		"id": current_nutri['id'],
		"full_name": current_nutri['full_name'],
		"email": current_nutri['email'],
		"crn": current_nutri['crn'],
		"spec": current_nutri['spec'],
		"age": current_nutri['age'],
		"hour": current_nutri['hour'],
		"birthday": str(current_nutri['birthday']),
		"url_image": current_nutri['url_image']
	}

	return jsonify(serializer), HTTPStatus.OK # <-- 200

@jwt_required()
def update_nutritionist():

	''' Update an attribute of logged nutritionist. email and password required'''

	nutri_data = request.get_json()

	try:
		nutri_update = (
			NutritionistModel
			.query
			.filter(NutritionistModel.email == nutri_data['email'])
			.first()
		)

		password = nutri_data.pop('password')
		nutri_update.password = password

		NutritionistModel.query.filter(NutritionistModel.email == nutri_data['email']).update(nutri_data)
		current_app.db.session.commit()

		return jsonify(nutri_update), HTTPStatus.OK # <-- 200

	except sqlalchemy.exc.InvalidRequestError:
		return {'error': 'Invalid fields.'}, HTTPStatus.BAD_REQUEST # <-- 400
	except KeyError as error:
		return {'error': f'You are missing this argument: {error.args[0]}.'}, HTTPStatus.BAD_REQUEST # <-- 400

@jwt_required()
def delete_nutritionist():

	'''Delete a looged nutritionist'''

	nutri_to_be_deleted = get_jwt_identity()
	nutri_data = (
		NutritionistModel
		.query
		.filter(NutritionistModel.email == nutri_to_be_deleted['email'])
		.first()
	)

	current_app.db.session.delete(nutri_data)
	current_app.db.session.commit()

	return {'msg': f'Nutritionist {nutri_data.full_name} was deleted!'}, HTTPStatus.OK # <--200


def get_nutrionist_by_name(name: str):

	''' Find a nutritionist by name'''
	
	try:
		found_nutri = (
			NutritionistModel
			.query
			.filter(NutritionistModel.full_name.ilike(f'%{name}%')) 
			.all()
		)

		if not found_nutri:
			raise NutritionistNotFoundError

		return jsonify(found_nutri), HTTPStatus.OK # <-- 200

	except NutritionistNotFoundError as error:
		return {'error': 'Nutritionist not found!'}, HTTPStatus.NOT_FOUND # <-- 404


def get_all_nutrionists():

	''' List all nutritionists in database'''

	try:
		nutri_data = (
			NutritionistModel
			.query
			.all()
		)

		serializer = [
			{
			"id": nutri.id,
			"full_name": nutri.full_name,
			"email": nutri.email,
			"crn": nutri.crn,
			"spec": nutri.spec,
			"age": nutri.age,
			"hour": nutri.hour,
			"birthday": nutri.birthday,
			"url_image": nutri.url_image
		} for nutri in nutri_data
		]

		if len(serializer) == 0:
			raise EmptyListError

		return jsonify(serializer), HTTPStatus.OK # <-- 200

	except EmptyListError as error:
		return {'message': 'There is no nutritionists here!'}, HTTPStatus.OK # <-- 200


@jwt_required()
def update_image():

	''' Update image of avatar '''

	found_user = get_jwt_identity()

	nutri_data = request.get_json()

	try:
		nutri_update = (
			NutritionistModel
			.query
			.filter(NutritionistModel.email == found_user['email'])
			.first()
		)

		url_image = found_user.pop('url_image')
		nutri_update.url_image = url_image

		NutritionistModel.query.filter(NutritionistModel.email == found_user['email']).update(nutri_data)
		current_app.db.session.commit()

		return jsonify(nutri_update), HTTPStatus.OK # <-- 200

	except sqlalchemy.exc.InvalidRequestError:
		return {'error': 'Invalid fields.'}, HTTPStatus.BAD_REQUEST # <-- 400
	except KeyError as error:
		return {'error': f'You are missing this argument: {error.args[0]}.'}, HTTPStatus.BAD_REQUEST # <-- 400


def get_nutri_rating(nutri_full_name: str):
   
	nutri = (
		NutritionistModel
		.query
		.filter(NutritionistModel.full_name.ilike(f'%{nutri_full_name}%'))
		.first()
	)

	if not nutri:
		{'error': 'Nutri Not Found'}, HTTPStatus.NOT_FOUND # <-- 404

	rating = None

	ratings = NutriRatingModel.query.filter(NutriRatingModel.nutri_id == nutri.id).all()

	if ratings:
		ratings = [item.rating for item in ratings]
		rating = reduce((lambda a, b: a + b), ratings) / len(ratings)
	else:
		return {'message': f'{nutri_full_name} was not rated yet. Be the first!'}, HTTPStatus.OK # <-- 200

	return {"rating": rating}, HTTPStatus.OK # <-- 200

#eof