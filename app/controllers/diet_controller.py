from flask import request, current_app, jsonify
from app.models.category_model import CategoryModel
from flask_jwt_extended import jwt_required,get_jwt_identity
from app.models.diet_model import DietModel
from app.models.food_model import FoodModel
from app.models.meal_model import MealModel
from app.models.nutritionist_model import NutritionistModel
from app.models.order_model import OrderModel
from app.models.user_model import UserModel


@jwt_required()
def get_all_diet_of_user():
    jwt_user = get_jwt_identity()

    diets = (
      DietModel
      .query
      .filter(DietModel.user_id == jwt_user['id'])
      .all()
    )
    
    if not diets:
      return {"err":"Diet not found."},404

    if len(diets) == 0:
        return {"msg": "Diet empty."},200

    return jsonify([{
        "id": item.id,
        "user_id":item.user_id,
        "nutri":NutritionistModel.query.filter_by(id=item.nutri_id).first(),
        "description":item.description,
        "meals":[{
        "id": food.id,
        "meal_id": MealModel.query.filter(MealModel.food_id == food.id).first().id,
        "quanty":MealModel.query.filter(MealModel.food_id == food.id).first().quanty,
        "description": food.description,
        'category': current_app.db.session.query(CategoryModel).select_from(FoodModel).join(CategoryModel).filter(CategoryModel.id == food.category_id).all()[0].category,
        "base_qty":food.base_qty,
        'base_unit':food.base_unit,
        'protein':food.protein,
        'lipid':food.lipid,
        'cholesterol':food.cholesterol,
        'carbohydrate':food.carbohydrate,
        'fiber':food.fiber,
        'calcium':food.calcium,
        'iron':food.iron,
        'sodium':food.sodium,
        'potassium':food.potassium,
        'energy':food.energy} for food in current_app.db.session.query(FoodModel).select_from(MealModel).join(DietModel).join(FoodModel).filter(DietModel.id == item.id).yield_per(1).all()]
      }for item in diets]), 200

@jwt_required()
def delete_diet(diet_id):
    jwt_user = get_jwt_identity()

    diet = DietModel.query.filter_by(id=diet_id).first()

    if not diet:
        return {"err":"Diet not found."},404

    if jwt_user['id'] != diet.user_id:
      return {'err':"Unauthorized"},404

    meals = MealModel.query.filter_by(diet_id = diet_id).all()
    
    for meal in meals:
      current_app.db.session.delete(meal)
    current_app.db.session.commit()

    current_app.db.session.delete(diet)
    current_app.db.session.commit()
    return "", 204

@jwt_required()
def create_diet():
    data = request.get_json()

    jwt_user = get_jwt_identity()

    if not "crn" in jwt_user.keys():
        data['user_id'] = jwt_user['id']

    if "crn" in jwt_user.keys():
        data['nutri_id'] = jwt_user['id']
        if not "user_id" in data:
          return {'err':"user_id is required."},404
        user = UserModel.query.filter_by(id=data['user_id']).first()
        if not user:
          return {'err':"User not found."},404
        order = OrderModel.query.filter_by(user_id = data['user_id'],nutri_id = jwt_user['id'], status = "accepted").first()
        if not order:
          return {'err': "You cannot create a diet for this user"},404

    try:

      newdiet = DietModel(**data)

      current_app.db.session.add(newdiet)
      current_app.db.session.commit()

      return {"id":newdiet.id,"nutri_id":newdiet.nutri_id,"user_id":newdiet.user_id},201

    except KeyError and TypeError:
      return {"err": "Only the keys nutri_id and user_id are accepted."},404

@jwt_required()
def add_food_in_diet(diet_id):
    try:
      data = request.get_json()

      diet = DietModel.query.filter_by(id= diet_id).first()

      if not diet:
        return {"err":"Diet not found."},404

      jwt_user = get_jwt_identity()

      if not "crn" in jwt_user.keys():
        if jwt_user['id'] != diet.user_id:
          return {'err':"Unauthorized"},404

      if "crn" in jwt_user.keys():
        diet = DietModel.query.filter_by(nutri_id = jwt_user['id'], id = diet_id).first()
        if not diet:
          return {'err': "You cannot add food for this diet"},404

      food = FoodModel.query.filter_by(id=data['food_id']).first()

      if not food:
        return {"err":"Food not found."},404
      
      if data['quanty'] <= 0:
        return {"err":"Quanty must be greater than zero."},404

      refeicao = MealModel(food_id = food.id, diet_id = diet.id,quanty = data['quanty'])
      

      current_app.db.session.add(refeicao)
      current_app.db.session.commit()

      return {"id":refeicao.id,"food_id": refeicao.food_id,"quanty": refeicao.quanty},202

    except KeyError:
      return {"err": "Only the food_id key is accepted."},404

@jwt_required()
def remove_food_in_diet(meal_id):

    meal = MealModel.query.filter_by(id=meal_id).first()

    if meal == None:
      return {"err": "Meal not found!"},404

    diet = (
      DietModel
      .query
      .get(meal.diet_id)
    )

    jwt_user = get_jwt_identity()

    if jwt_user['id'] != diet.user_id:
      return {'err':"Unauthorized"},404

    current_app.db.session.delete(meal)
    current_app.db.session.commit()
    return "", 204

@jwt_required()
def update_food_in_meal(meal_id):
    data = request.get_json()
    
    if not 'quanty' in data or len(data) != 1:
      return {"err" : "Only key quanty accept."},404
    
    meal = MealModel.query.filter_by(id=meal_id).first()

    if meal == None:
      return {"err": "Meal not found!"},404

    diet = (
      DietModel
      .query
      .get(meal.diet_id)
    )

    jwt_user = get_jwt_identity()

    if jwt_user['id'] != diet.user_id:
      return {'err':"Unauthorized"},404

    setattr(meal, 'quanty', data['quanty'] + meal.quanty)

    current_app.db.session.add(meal)
    current_app.db.session.commit()

    return "",204