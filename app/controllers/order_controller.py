from datetime import datetime
from flask import request, jsonify, current_app
from http import HTTPStatus

from flask_jwt_extended import jwt_required, get_jwt_identity
from app.models.order_model import OrderModel
from app.exceptions.order_exceptions import WrongKeysSended, ConflitError

from app.models.nutritionist_model import NutritionistModel
from app.models.user_model import UserModel


@jwt_required()
def create_order():
    try:
        user = get_jwt_identity()
        data = request.json
        data["user_id"] = user["id"]
        OrderModel.validate(**data)

        order = OrderModel(**data)
        order.comments = 'User:' + data['comments'] + ';'
        order.created_at = datetime.now()

        current_app.db.session.add(order)
        current_app.db.session.commit()
        data = OrderModel.validate(**data)
        
        nutri = NutritionistModel.query.filter_by(id=data['nutri_id']).first()
        user = UserModel.query.filter_by(id=data['user_id']).first()
        comment = data['comments']

        UserModel.send_email(nutri.email,"Novo pedido recebido !!",user.name.title(),comment)

        return jsonify(order), HTTPStatus.CREATED

    except WrongKeysSended as e:
        return jsonify({"error": str(e)}),HTTPStatus.BAD_REQUEST


@jwt_required()
def get_all():
    user = get_jwt_identity()
    if "crn" in user.keys():
        order = OrderModel.query.filter_by(nutri_id=user["id"]).all()    
    else:
        order = OrderModel.query.filter_by(user_id=user["id"]).all()
    if not order:
        return {"msg": "No order found"}, HTTPStatus.NOT_FOUND

    return jsonify(order), HTTPStatus.OK


@jwt_required()
def update_order(order_id: int):
    try:
        data = request.json
        OrderModel.validate_update(**data)

        user = get_jwt_identity()
        if "crn" in user.keys():
            order = OrderModel.query.filter_by(id=order_id, nutri_id=user["id"]).first()    
            if order:
                order.comments += 'Nutri:'+ data['comments'] + ";"
        else:
            order = OrderModel.query.filter_by(id=order_id, user_id=user["id"]).first()
            if order:
                order.comments += 'User:'+ data['comments'] + ";"

        if not order:
            return {"msg": "Order not found"}, HTTPStatus.NOT_FOUND
        order.status = data['status']

        current_app.db.session.commit()
    except WrongKeysSended as e:
        return jsonify({"error": str(e)}),HTTPStatus.BAD_REQUEST

    return "", HTTPStatus.NO_CONTENT
    

@jwt_required()
def delete_order(order_id: int):
    user = get_jwt_identity()
    if "crn" in user.keys():
            order = OrderModel.query.filter_by(id=order_id, nutri_id=user["id"]).first()  
    else:
            order = OrderModel.query.filter_by(id=order_id, user_id=user["id"]).first()

    if not order:
        return {"msg": "Order not found"}, HTTPStatus.NOT_FOUND
    
    current_app.db.session.delete(order)
    current_app.db.session.commit()

    return "", HTTPStatus.NO_CONTENT